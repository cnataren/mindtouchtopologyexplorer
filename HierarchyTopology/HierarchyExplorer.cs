﻿using System;
using System.Collections.Generic;
using System.Linq;
using MindTouch.Dream;
using MindTouch.Xml;

namespace HierarchyTopology {
    public class HierarchyExplorer {

        //--- Fields ---
        private readonly Plug _api;
        private const string ARTICLE = "article:";

        //--- Constructor ---
        public HierarchyExplorer(Plug api) {
            _api = api;
        }

        public IEnumerable<PageInfo> GetTopology(string pageroot, uint depth) {
            return GetPagesTopologyInformation(pageroot, depth);
        }

        private IEnumerable<PageInfo> GetPagesTopologyInformation(string p, uint depth) {
            return GetPagesTopologyInformationHelper(p, depth, 0, new List<PageInfo>());
        }

        private IEnumerable<PageInfo> GetPagesTopologyInformationHelper(string p, uint depth, uint counter, List<PageInfo> pageInfos) {
            var pageDoc = _api.At("@api", "deki", "pages", p).Get().ToDocument();
            var subpagesDoc = _api.At("@api", "deki", "pages", pageDoc["@id"].AsText, "subpages").Get().ToDocument();
            var subpages = subpagesDoc[".//page.subpage"].ToList();
            var pageId = pageDoc["@id"].AsText;
            var articleType = GetArticleType(pageDoc);
            pageInfos.Add(new PageInfo(pageId, pageDoc[".//path"].AsText, subpages.Count, articleType, counter, pageDoc));
            counter++;
            if(counter == depth) {
                 return pageInfos;
             }
            foreach(var page in subpages) {
                GetPagesTopologyInformationHelper(page["@id"].AsText, depth, counter, pageInfos);
            }
            return pageInfos;
        }

        private static string GetArticleType(XDoc pageDoc) {
            var tags = pageDoc["//tags"];
            var tagCount = tags["@count"].AsInt;
            if(tagCount <= 0) {
                return "";
            }
            foreach(var t in tags[".//tag"].Select(tag => tag["@value"].AsText).Where(t => t.StartsWithInvariantIgnoreCase(ARTICLE))) {
                return t.Substring(ARTICLE.Length);
            }
            return "";
        }
    }
}
