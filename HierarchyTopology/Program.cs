﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using MindTouch.Deki.Util.Csv;
using MindTouch.Dream;
using Mono.Options;

namespace HierarchyTopology {
    public class Program {
        public static void Main(string[] args) {

            // Validate the command line parameters
            var hostname = "";
            var startPage = "";
            var username = "";
            var password = "";
            var depth = 0u;
            var optionSet = new OptionSet {
                { "hostname=", "The {HOSTNAME} to use", v => hostname = v },
                { "pageroot=", "The {PAGEID} of the root page we should start analyzing from", pageid => startPage = pageid },
                { "username=", "The {USERNAME} to authenticate", user => username = user },
                { "password=", "The {PASSWORD} to authenticate", pwd => password = pwd },
                { "depth=", "The number of levels we explore", levels => depth = UInt32.Parse(levels) }
            };
            optionSet.Parse(args);
            if(string.IsNullOrEmpty(hostname) || string.IsNullOrEmpty(startPage) ||
                string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) {
                ShowHelpBanner(optionSet);
                return;
            }

            // Explore!
            var hierarchyExplorer = new HierarchyExplorer(Plug.New(hostname).WithCredentials(username, password));
            var topology = hierarchyExplorer.GetTopology(startPage, depth <= 0 ? 1 : depth);

            // Write the results
            var stream = new StreamWriter(new FileStream(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "-csv-" + Guid.NewGuid().ToString() + ".csv", FileMode.Create));
            stream.WriteLine("level, pageid, uri, path, articletype, subpagescount");
            foreach(var page in topology.GroupBy(pi => pi.Level).SelectMany(@group => @group)) {
                stream.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5}", page.Level, page.PageId, page.UriUi, page.Path, page.ArticleType, page.SubpagesCount));
            }
            stream.Flush();
            stream.Close();

            // Compute and display the stats
            /*
            var statsProcessor = new StatisticsProcessor(topology);
            var stats = statsProcessor.GetStatistics();
            foreach(var stat in stats) {
                Console.WriteLine("level = {0}, min#pages = {1}, max#pages = {2}, average#pages = {3}", stat.Level, stat.Min, stat.Max, stat.Average);
            }
            */
        }

        private static void ShowHelpBanner(OptionSet options) {
            Console.WriteLine("MindTouch Topology Explorer v0.1");
            Console.WriteLine("Options:");
            options.WriteOptionDescriptions(Console.Out);
        }
    }
}
