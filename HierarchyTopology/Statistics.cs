﻿namespace HierarchyTopology {
    public class Statistics {

        //--- Fields ---
        public readonly uint Level;
        public readonly int Min;
        public readonly int Max;
        public readonly double Average;
        public readonly double StdDev;

        //--- Constructors ---
        public Statistics(uint level, int min, int max, double average, double stdDev) {
            Level = level;
            Min = min;
            Max = max;
            Average = average;
            StdDev = stdDev;
        }
    }
}
