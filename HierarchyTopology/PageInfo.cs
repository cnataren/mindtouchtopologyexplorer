﻿using System;
using MindTouch.Xml;

namespace HierarchyTopology {
    public class PageInfo {

        //--- Fields ---
        public readonly string ArticleType;
        public readonly int SubpagesCount;
        public readonly string Path;
        public readonly string PageId;
        public readonly XDoc Doc;
        public readonly uint Level;
        public readonly string UriUi;

        //--- Methods ---
        public PageInfo(string pageId, string path, int subpagesCount, string articleType, uint level, XDoc doc) {
            PageId = pageId;
            Path = path;
            ArticleType = articleType;
            SubpagesCount = subpagesCount;
            Level = level;
            UriUi = doc[".//uri.ui"].AsText;
            Doc = doc;
        }
    }
}
