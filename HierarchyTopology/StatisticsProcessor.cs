﻿using System.Collections.Generic;
using System.Linq;

namespace HierarchyTopology {
    public class StatisticsProcessor {

        //--- Fields ---
        private readonly IEnumerable<PageInfo> _pages;
 
        //--- Constructors ---
        public StatisticsProcessor(IEnumerable<PageInfo> pages) {
            _pages = pages;
        }

        //--- Methods ----
        public IEnumerable<Statistics> GetStatistics() {
            var pagesByLevel =_pages.GroupBy(pi => pi.Level);
            return (from level in pagesByLevel
                    let minSubpages = level.Min(pi => pi.SubpagesCount)
                    let maxSubpages = level.Max(pi => pi.SubpagesCount)
                    let averagePages = level.Average(pi => pi.SubpagesCount)
                    select new Statistics(level.Key, minSubpages, maxSubpages, averagePages, 0)).ToArray();
        } 
    }
}
